package com.ts.ProductsSpring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.dao.ProductDAO;
import com.model.Product;

@RestController
public class ProductController {

	@Autowired
	ProductDAO prodDAO;

	@RequestMapping("getProducts")
	public List<Product> getAllProducts() {
		return prodDAO.getAllProducts();
	}

	@RequestMapping("/showProduct")
	public Product showProduct() {

		Product product = new Product(101, "ABC", 590.00);
		return product;
	}

	@RequestMapping("/getAllProducts")
	public List<Product> showProducts() {

		List<Product> products = new ArrayList<>();
		products.add(new Product(101, "Apple", 80000));
		products.add(new Product(102, "Samsung", 45000));
		products.add(new Product(103, "One plus", 60000));

		return products;
	}

	@RequestMapping("/getProductById/{id}")
	public Product getProductById(@PathVariable("id") int id) {
		return prodDAO.getProductById(id);
	}

	@RequestMapping("/getProductByName/{name}")
	public Product getProductByName(@PathVariable("name") String name) {
		return prodDAO.getProductByName(name);
	}
	
	@PostMapping("/registerProduct")
    public Product registerProduct(@RequestBody Product product) {
        return prodDAO.updateProduct(product);
    }
	@PutMapping("/updateProduct")
    public Product updateProduct(@RequestBody Product product) {
        return prodDAO.updateProduct(product);
    }

}