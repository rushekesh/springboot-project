package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDAO {

	@Autowired
	ProductRepository productRepo;

	public List<Product> getAllProducts() {
		return productRepo.findAll();
	}

	public Product getProductById(int id) {
		Product prod = new Product(1, "Product", 99999.99);
		return productRepo.findById(id).orElse(prod);
	}

	public Product getProductByName(String name) {
		return productRepo.findByName(name);
	}

	public Product registerProduct(Product product) {
       return productRepo.save(product);
   }
	public Product updateProduct(Product product) {
        return productRepo.save(product);
	}

}